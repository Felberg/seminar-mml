from __future__ import division
from __future__ import absolute_import
from __future__ import print_function

import os
import models
import argparse
import tensorflow as tf
import multiprocessing

import numpy as np

def get_args():
    parser = argparse.ArgumentParser(description="Train DepthModel")
    parser.add_argument("--weights", type=str, default="d:/Uni/MML/washington/depth_weights/weights.34-0.83.hdf5")

    return parser.parse_args()

if __name__ == '__main__':
    args = get_args()
    weights = args.weights
    d_model = models.DepthModel(weights=weights)
    core_layers = d_model.layers[:-4]
    no_top = tf.keras.Sequential(core_layers)
    no_top.summary()
    no_top.save(args.weights.replace("weights.", "weights_notop."))


