from __future__ import absolute_import
from __future__ import division
from __future__ import print_function

import tensorflow as tf
from tensorflow.python.keras import backend
from tensorflow.python.keras.engine import training
from tensorflow.python.keras.utils import data_utils
from tensorflow.python.keras.utils import layer_utils
from tensorflow.python.lib.io import file_io
"""Model reimplementations based on the following papers"""
"""[1] 'LEARNING TO FUSE LATENT REPRESENTATIONS FOR MULTIMODAL DATA', Oyebade K. Oyedotun et al. https://orbilu.uni.lu/bitstream/10993/40568/1/Oyebade_ICASSP2019.pdf"""
"""[2] 'Learning and Fusing Multimodal Deep Features for AcousticScene Categorization' Yifang Yin et al. https://dl.acm.org/doi/pdf/10.1145/3240508.3240631"""


def DepthModel(include_top=True,
               weights=None,
               input_tensor=None,
               input_shape=[64, 64, 1],
               pooling=None,
               classes=51):
    """Model trained to classify depth images as in [1]
    
    Args:
    """

    # layers = VersionAwareLayers()
    layers = tf.keras.layers

    if not (weights is None or file_io.file_exists(weights)):
        raise ValueError('The `weights` argument should be either '
                         '`None` (random initialization) '
                         'or the path to the weights file to be loaded.')

    bn_axis = 3

    if input_tensor is None:
        img_input = layers.Input(shape=input_shape)
    else:
        if not backend.is_keras_tensor(input_tensor):
            img_input = layers.Input(tensor=input_tensor, shape=input_shape)
        else:
            img_input = input_tensor

    x = layers.Conv2D(10, (6, 6),
                      activation='relu',
                      padding='same',
                      name='conv1')(img_input)
    x = layers.Conv2D(20, (6, 6),
                      activation='relu',
                      padding='same',
                      name='conv2')(x)
    x = layers.BatchNormalization(axis=bn_axis, epsilon=1.001e-5,
                                  name='bn1')(x)
    x = layers.MaxPooling2D((2, 2), strides=(2, 2), name='pool1')(x)

    x = layers.Conv2D(30, (6, 6),
                      activation='relu',
                      padding='same',
                      name='conv3')(x)
    x = layers.Conv2D(40, (6, 6),
                      activation='relu',
                      padding='same',
                      name='conv4')(x)
    x = layers.BatchNormalization(axis=bn_axis, epsilon=1.001e-5,
                                  name='bn2')(x)
    x = layers.MaxPooling2D((2, 2), strides=(2, 2), name='pool2')(x)

    x = layers.Conv2D(50, (6, 6),
                      activation='relu',
                      padding='same',
                      name='conv5')(x)
    x = layers.BatchNormalization(axis=bn_axis, epsilon=1.001e-5,
                                  name='bn3')(x)
    # x = layers.MaxPooling2D((4, 4), strides=(4, 4), name='pool3')(x)
    x = layers.MaxPooling2D((2, 2), strides=(4, 4), name='pool3')(
        x
    )  # TODO: Test if this makes a difference.. architecture unclear in paper

    if include_top:
        x = layers.Flatten(name='flatten')(x)
        x = layers.Dense(300, activation='relu', name='fc1')(x)
        x = layers.Dense(300, activation='relu', name='fc2')(x)
        x = layers.Dense(classes, activation='softmax', name='predictions')(x)
    else:
        if pooling == 'avg':
            x = layers.GlobalAveragePooling2D(name='avg_pool')(x)
        elif pooling == 'max':
            x = layers.GlobalMaxPooling2D(name='max_pool')(x)

    # Ensure that the model takes into account
    # any potential predecessors of `input_tensor`.
    if input_tensor is not None:
        inputs = layer_utils.get_source_inputs(input_tensor)
    else:
        inputs = img_input

    # Create model.
    model = training.Model(inputs, x, name='depth')

    # Load weights.
    if weights is not None:
        model.load_weights(weights)

    return model


def Model1D(include_top=True,
            weights=None,
            input_tensor=None,
            input_shape=None,
            classes=15):
    """Model as proposed in [2] to classify 1D audio

    Arguments:
    """

    layers = VersionAwareLayers()

    if not (weights is None or file_io.file_exists(weights)):
        raise ValueError('The `weights` argument should be either '
                         '`None` (random initialization) '
                         'or the path to the weights file to be loaded.')

    bn_axis = 2

    if input_tensor is None:
        aud_input = layers.Input(shape=input_shape)
    else:
        if not backend.is_keras_tensor(input_tensor):
            aud_input = layers.Input(tensor=input_tensor, shape=input_shape)
        else:
            aud_input = input_tensor

    x = layers.Conv1D(64,
                      16,
                      strides=2,
                      activation='relu',
                      padding='same',
                      name='conv1')(aud_input)
    x = layers.BatchNormalization(axis=bn_axis, epsilon=1.001e-5,
                                  name='bn1')(x)
    x = layers.MaxPool1D(16, strides=4, name='pool1')(x)

    x = layers.Conv1D(64,
                      16,
                      strides=2,
                      activation='relu',
                      padding='same',
                      name='conv2')(x)
    x = layers.BatchNormalization(axis=bn_axis, epsilon=1.001e-5,
                                  name='bn2')(x)
    x = layers.MaxPool1D(16, strides=4, name='pool2')(x)

    x = layers.Conv1D(128,
                      16,
                      strides=2,
                      activation='relu',
                      padding='same',
                      name='conv3')(x)
    x = layers.BatchNormalization(axis=bn_axis, epsilon=1.001e-5,
                                  name='bn3')(x)
    x = layers.Conv1D(128,
                      16,
                      strides=2,
                      activation='relu',
                      padding='same',
                      name='conv4')(x)
    x = layers.BatchNormalization(axis=bn_axis, epsilon=1.001e-5,
                                  name='bn4')(x)
    x = layers.Conv1D(256,
                      8,
                      strides=2,
                      activation='relu',
                      padding='same',
                      name='conv5')(x)
    x = layers.BatchNormalization(axis=bn_axis, epsilon=1.001e-5,
                                  name='bn5')(x)
    x = layers.MaxPool1D(8, strides=4, name='pool5')(x)

    drop_rate = 0.7

    x = layers.Conv1D(256,
                      8,
                      strides=2,
                      activation='relu',
                      padding='same',
                      name='conv6')(x)
    x = layers.BatchNormalization(axis=bn_axis, epsilon=1.001e-5,
                                  name='bn6')(x)
    x = layers.Dropout(drop_rate, name='drop1')(x)
    x = layers.Conv1D(512,
                      8,
                      strides=2,
                      activation='relu',
                      padding='same',
                      name='conv7')(x)
    x = layers.BatchNormalization(axis=bn_axis, epsilon=1.001e-5,
                                  name='bn7')(x)
    x = layers.Dropout(drop_rate, name='drop2')(x)
    x = layers.Conv1D(15,
                      8,
                      strides=2,
                      activation='relu',
                      padding='same',
                      name='conv8')(x)
    x = layers.BatchNormalization(axis=bn_axis, epsilon=1.001e-5,
                                  name='bn8')(x)
    x = layers.Dropout(drop_rate, name='drop3')(x)

    if include_top:
        x = layers.GlobalMaxPooling1D()(x)
        x = layers.Softmax()(x)

    model = training.Model(inputs, x, name='1d')

    # Load weights.
    if weights is not None:
        model.load_weights(weights)

    return model


def Model2D(include_top=True,
            weights=None,
            input_tensor=None,
            input_shape=None,
            classes=15):

    layers = VersionAwareLayers()
    bn_axis = 3

    if not (weights is None or file_io.file_exists(weights)):
        raise ValueError('The `weights` argument should be either '
                         '`None` (random initialization) '
                         'or the path to the weights file to be loaded.')

    if input_tensor is None:
        aud_input = layers.Input(shape=input_shape)
    else:
        if not backend.is_keras_tensor(input_tensor):
            aud_input = layers.Input(tensor=input_tensor, shape=input_shape)
        else:
            aud_input = input_tensor

    x = layers.Conv2D(64, (5, 5),
                      strides=(2, 2),
                      activation='relu',
                      padding='same',
                      name='conv1')(aud_input)
    x = layers.BatchNormalization(axis=bn_axis, epsilon=1.001e-5,
                                  name='bn1')(x)
    x = layers.MaxPool2D((2, 2), strides=(2, 2), name='pool1')(x)

    x = layers.Conv2D(64, (5, 5),
                      strides=(2, 2),
                      activation='relu',
                      padding='same',
                      name='conv2')(x)
    x = layers.BatchNormalization(axis=bn_axis, epsilon=1.001e-5,
                                  name='bn2')(x)
    x = layers.MaxPool2D((2, 2), strides=(2, 2), name='pool2')(x)

    x = layers.Conv2D(128, (5, 5),
                      strides=(2, 2),
                      activation='relu',
                      padding='same',
                      name='conv3')(x)
    x = layers.BatchNormalization(axis=bn_axis, epsilon=1.001e-5,
                                  name='bn3')(x)
    x = layers.Conv2D(128, (5, 5),
                      strides=(2, 2),
                      activation='relu',
                      padding='same',
                      name='conv4')(x)
    x = layers.BatchNormalization(axis=bn_axis, epsilon=1.001e-5,
                                  name='bn4')(x)

    x = layers.Flatten()(x)
    x = layers.Dense(256, activation='relu', name='fc1')(x)
    x = layers.Dropout(0.5, name='drop1')

    if include_top:
        x = layers.Dense(15, activation='softmax', name='predictions')(x)

    # Ensure that the model takes into account
    # any potential predecessors of `input_tensor`.
    if input_tensor is not None:
        inputs = layer_utils.get_source_inputs(input_tensor)
    else:
        inputs = aud_input

    model = training.Model(inputs, x, name='2d')

    # Load weights.
    if weights is not None:
        model.load_weights(weights)

    return model


def Model3D(include_top=True,
            weights=None,
            input_tensor=None,
            input_shape=None,
            classes=15):

    layers = VersionAwareLayers()
    bn_axis = 4

    if not (weights is None or file_io.file_exists(weights)):
        raise ValueError('The `weights` argument should be either '
                         '`None` (random initialization) '
                         'or the path to the weights file to be loaded.')

    if input_tensor is None:
        aud_input = layers.Input(shape=input_shape)
    else:
        if not backend.is_keras_tensor(input_tensor):
            aud_input = layers.Input(tensor=input_tensor, shape=input_shape)
        else:
            aud_input = input_tensor

    x = layers.Conv3D(64, (5, 5, 5),
                      strides=(1, 2, 2),
                      activation='relu',
                      padding='same',
                      name='conv1')(aud_input)
    x = layers.BatchNormalization(axis=bn_axis, epsilon=1.001e-5,
                                  name='bn1')(x)
    x = layers.MaxPool2D((2, 2, 2), strides=(2, 2, 2), name='pool1')(x)

    x = layers.Conv3D(64, (5, 5, 5),
                      strides=(1, 2, 2),
                      activation='relu',
                      padding='same',
                      name='conv2')(x)
    x = layers.BatchNormalization(axis=bn_axis, epsilon=1.001e-5,
                                  name='bn2')(x)
    x = layers.MaxPool2D((2, 2, 2), strides=(2, 2, 2), name='pool2')(x)

    x = layers.Conv3D(128, (5, 5, 5),
                      strides=(1, 2, 2),
                      activation='relu',
                      padding='same',
                      name='conv3')(x)
    x = layers.BatchNormalization(axis=bn_axis, epsilon=1.001e-5,
                                  name='bn3')(x)
    x = layers.Conv3D(128, (5, 5, 5),
                      strides=(1, 2, 2),
                      activation='relu',
                      padding='same',
                      name='conv4')(x)
    x = layers.BatchNormalization(axis=bn_axis, epsilon=1.001e-5,
                                  name='bn4')(x)

    x = layers.Flatten()(x)
    x = layers.Dense(256, activation='relu', name='fc1')(x)
    x = layers.Dropout(0.5)(x)

    if include_top:
        x = layers.Dense(15, activation='softmax', name='predictions')(x)

    # Ensure that the model takes into account
    # any potential predecessors of `input_tensor`.
    if input_tensor is not None:
        inputs = layer_utils.get_source_inputs(input_tensor)
    else:
        inputs = aud_input

    model = training.Model(inputs, x, name='3d')

    # Load weights.
    if weights is not None:
        model.load_weights(weights)

    return model
