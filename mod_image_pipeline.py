from __future__ import absolute_import
from __future__ import division
from __future__ import print_function

# Copyright 2020 The TensorFlow Authors. All Rights Reserved.
#
# Licensed under the Apache License, Version 2.0 (the "License");
# you may not use this file except in compliance with the License.
# You may obtain a copy of the License at
#
#     http://www.apache.org/licenses/LICENSE-2.0
#
# Unless required by applicable law or agreed to in writing, software
# distributed under the License is distributed on an "AS IS" BASIS,
# WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
# See the License for the specific language governing permissions and
# limitations under the License.
# ==============================================================================
"""Keras image dataset loading utilities."""
"""Modified slightly by me...."""
"""Original from: https://github.com/tensorflow/tensorflow/blob/v2.2.0/tensorflow/python/keras/preprocessing/image_pipeline.py"""
# pylint: disable=g-classes-have-attributes
import multiprocessing
import os

import numpy as np

import tensorflow as tf
from tensorflow.python.data.ops import dataset_ops
from tensorflow.python.keras.layers.preprocessing import image_preprocessing
from tensorflow.python.ops import array_ops
from tensorflow.python.ops import image_ops
from tensorflow.python.ops import io_ops
from tensorflow.python.ops import math_ops

max_depth = 38.0
"""Modified... Only accepts png now..."""
WHITELIST_FORMATS = ('.png')


def dataset_from_directory(
    directories,
    labels='inferred',
    label_mode='categorical',
    class_names=None,
    color_modes=['grayscale', 'rgb'],
    # batch_size=32,
    image_sizes=[(64, 64), (128, 128)],
    shuffle=True,
    seed=None,
    num_samples=None,
    per_label_shuffle=False,
    interpolation=['nearest', 'bilinear'],
    follow_links=False):
    """Modified to work with a list of directories. Dataset takes the form: ({img_dir1, img_dir2, ...}, labels)"""
    """Generates a Dataset from image files in a directory.

  If your directory structure is:

  ```
  main_directory/
  ...class_a/
  ......a_image_1.jpg
  ......a_image_2.jpg
  ...class_b/
  ......b_image_1.jpg
  ......b_image_2.jpg
  ```

  Then calling `from_directory(main_directory, labels='inferred')`
  will return a Dataset that yields batches of images from
  the subdirectories `class_a` and `class_b`, together with labels
  0 and 1 (0 corresponding to class_a and 1 corresponding to class_b).

  Supported image formats: jpeg, png, bmp, gif.
  Animated gifs are truncated to the first frame.

  Arguments:
    directory: Directory where the data is located.
        If `labels` is "inferred", it should contain
        subdirectories, each containing images for a class.
        Otherwise, the directory structure is ignored.
    labels: Either "inferred"
        (labels are generated from the directory structure),
        or a list/tuple of integer labels of the same size as the number of
        image files found in the directory. Labels should be sorted according
        to the alphanumeric order of the image file paths
        (obtained via `os.walk(directory)` in Python).
    label_mode:
        - 'int': means that the labels are encoded as integers
            (e.g. for `sparse_categorical_crossentropy` loss).
        - 'categorical' means that the labels are
            encoded as a categorical vector
            (e.g. for `categorical_crossentropy` loss).
        - 'binary' means that the labels (there can be only 2)
            are encoded as `float32` scalars with values 0 or 1
            (e.g. for `binary_crossentropy`).
        - None (no labels).
    class_names: Only valid if "labels" is "inferred". This is the explict
        list of class names (must match names of subdirectories). Used
        to control the order of the classes
        (otherwise alphanumerical order is used).
    color_mode: One of "grayscale", "rgb", "rgba". Default: "rgb".
        Whether the images will be converted to
        have 1, 3, or 4 channels.
    batch_size: Size of the batches of data. Default: 32.
    image_size: Size to resize images to after they are read from disk.
        Defaults to `(256, 256)`.
        Since the pipeline processes batches of images that must all have
        the same size, this must be provided.
    shuffle: Whether to shuffle the data. Default: True.
        If set to False, sorts the data in alphanumeric order.
    seed: Optional random seed for shuffling and transformations.
    validation_split: Optional float between 0 and 1,
        fraction of data to reserve for validation.
    subset: One of "training" or "validation".
        Only used if `validation_split` is set.
    interpolation: String, the interpolation method used when resizing images.
      Defaults to `bilinear`. Supports `bilinear`, `nearest`, `bicubic`,
      `area`, `lanczos3`, `lanczos5`, `gaussian`, `mitchellcubic`.
    follow_links: Whether to visits subdirectories pointed to by symlinks.
        Defaults to False.

  Returns:
    A `tf.data.Dataset` object.
      - If `label_mode` is None, it yields `float32` tensors of shape
        `(batch_size, image_size[0], image_size[1], num_channels)`,
        encoding images (see below for rules regarding `num_channels`).
      - Otherwise, it yields a tuple `(images, labels)`, where `images`
        has shape `(batch_size, image_size[0], image_size[1], num_channels)`,
        and `labels` follows the format described below.

    Rules regarding labels format:
      - if `label_mode` is `int`, the labels are an `int32` tensor of shape
        `(batch_size,)`.
      - if `label_mode` is `binary`, the labels are a `float32` tensor of
        1s and 0s of shape `(batch_size, 1)`.
      - if `label_mode` is `categorial`, the labels are a `float32` tensor
        of shape `(batch_size, num_classes)`, representing a one-hot
        encoding of the class index.

    Rules regarding number of channels in the yielded images:
      - if `color_mode` is `grayscale`,
        there's 1 channel in the image tensors.
      - if `color_mode` is `rgb`,
        there are 3 channel in the image tensors.
      - if `color_mode` is `rgba`,
        there are 4 channel in the image tensors.
  """
    if labels != 'inferred':
        if not isinstance(labels, (list, tuple)):
            raise ValueError(
                '`labels` argument should be a list/tuple of integer labels, of '
                'the same size as the number of image files in the target '
                'directory. If you wish to infer the labels from the subdirectory '
                'names in the target directory, pass `labels="inferred"`. '
                'If you wish to get a dataset that only contains images '
                '(no labels), pass `labels=None`.')
        if class_names:
            raise ValueError(
                'You can only pass `class_names` if the labels are '
                'inferred from the subdirectory names in the target '
                'directory (`labels="inferred"`).')
    if label_mode not in {'int', 'categorical', 'binary', None}:
        raise ValueError(
            '`label_mode` argument must be one of "int", "categorical", "binary", '
            'or None. Received: %s' % (label_mode, ))
    num_channels = [0 for i in color_modes]
    for i, color_mode in enumerate(color_modes):
        if color_mode == 'rgb':
            num_channels[i] = 3
        elif color_mode == 'rgba':
            num_channels[i] = 4
        elif color_mode == 'grayscale':
            num_channels[i] = 1
        else:
            raise ValueError(
                '`color_mode` must be one of {"rbg", "rgba", "grayscale"}. '
                'Received: %s' % (color_mode, ))

    interpolation = [
        image_preprocessing.get_interpolation(i) for i in interpolation
    ]

    inferred_class_names = [list() for i in directories]
    for i, directory in enumerate(directories):
        for subdir in sorted(os.listdir(directory)):
            if os.path.isdir(os.path.join(directory, subdir)):
                inferred_class_names[i].append(subdir)
    if not class_names:
        class_names = inferred_class_names[0]
    else:
        if set(class_names) != set(inferred_class_names):
            raise ValueError(
                'The `class_names` passed did not match the '
                'names of the subdirectories of the target directory. '
                'Expected: %s, but received: %s' %
                (inferred_class_names, class_names))
    for inf in inferred_class_names:
        if set(class_names) != set(inf):
            raise ValueError("Mismatching classes found in directories")
    class_indices = dict(zip(class_names, range(len(class_names))))

    if label_mode == 'binary' and len(class_names) != 2:
        raise ValueError(
            'When passing `label_mode="binary", there must exactly 2 classes. '
            'Found the following classes: %s' % (class_names, ))

    # Build an index of the images
    # in the different class subfolders.
    all_image_paths = []
    for directory in directories:
        pool = multiprocessing.pool.ThreadPool()
        results = []
        filenames = []
        for dirpath in (os.path.join(directory, subdir)
                        for subdir in class_names):
            results.append(
                pool.apply_async(list_labeled_images_in_directory,
                                 (dirpath, class_indices, follow_links)))
        labels_list = []
        for res in results:
            partial_labels, partial_filenames = res.get()
            labels_list.append(partial_labels)
            filenames += partial_filenames
        if labels != 'inferred':
            if len(labels) != len(filenames):
                raise ValueError(
                    'Expected the lengths of `labels` to match the number '
                    'of images in the target directory. len(labels) is %s '
                    'while we found %s images in %s.' %
                    (len(labels), len(filenames), directory))
        else:
            i = 0
            labels = np.zeros((len(filenames), ), dtype='int32')
            for partial_labels in labels_list:
                labels[i:i + len(partial_labels)] = partial_labels
                i += len(partial_labels)

        print('Found %d images belonging to %d classes.' %
              (len(filenames), len(class_names)))
        pool.close()
        pool.join()

        image_paths = [os.path.join(directory, fname) for fname in filenames]
        all_image_paths.append(image_paths)

    if seed is None:
        seed = np.random.randint(1e6)

    if num_samples:
        # only take a few samples evenly from each class
        num_per_class = [0 for i in range(len(class_names))]
        for x in labels:
            num_per_class[x] += 1

        pick_per_class = int(num_samples / len(class_names)) + 1
        rem = num_samples % len(class_names)

        c = 0
        m = 0
        picks = []
        np.random.seed(seed)
        for i, n in enumerate(num_per_class):
            m += n
            if i == rem:
                pick_per_class -= 1
            p_in = np.random.random_integers(c, m - 1, pick_per_class)
            if per_label_shuffle:
                np.random.shuffle(p_in)
            picks.extend(p_in)

            c = m

        picked_images = [[] for i in all_image_paths]
        picked_labels = []
        for p in picks:
            for i, images in enumerate(all_image_paths):
                picked_images[i].append(images[p])

            picked_labels.append(labels[p])

        all_image_paths = picked_images
        labels = picked_labels

    if shuffle:
        # Shuffle globally to erase macro-structure
        # (the dataset will be further shuffled within a local buffer
        # at each iteration)

        for images in all_image_paths:
            rng = np.random.RandomState(seed)
            rng.shuffle(images)

        rng = np.random.RandomState(seed)
        rng.shuffle(labels)

    dataset = paths_and_labels_to_dataset(image_paths=all_image_paths,
                                          image_size=image_sizes,
                                          num_channels=num_channels,
                                          labels=labels,
                                          label_mode=label_mode,
                                          num_classes=len(class_names),
                                          interpolation=interpolation)
    # if shuffle:
    # Shuffle locally at each iteration
    # dataset = dataset.shuffle(buffer_size=batch_size * 8, seed=seed)
    # dataset = dataset.batch(batch_size)
    return dataset, len(all_image_paths[0])
    # return dataset


def paths_and_labels_to_dataset(image_paths, image_size, num_channels, labels,
                                label_mode, num_classes, interpolation):
    """Modified to work with list of image_paths"""
    """Constructs a dataset of images and labels."""
    # TODO(fchollet): consider making num_parallel_calls settable
    ds_list = []
    for i, images in enumerate(image_paths):
        path_ds = dataset_ops.Dataset.from_tensor_slices(images)
        img_ds = path_ds.map(lambda x: path_to_image(x, image_size[
            i], num_channels[i], interpolation[i]))
        ds_list.append(img_ds)
    ds_tuple = tuple(ds_list)
    img_ds = dataset_ops.Dataset.zip(ds_tuple)
    if label_mode:
        label_ds = dataset_ops.Dataset.from_tensor_slices(labels)
        if label_mode == 'binary':
            label_ds = label_ds.map(lambda x: array_ops.expand_dims(
                math_ops.cast(x, 'float32'), axis=-1))
        elif label_mode == 'categorical':
            label_ds = label_ds.map(
                lambda x: array_ops.one_hot(x, num_classes))
        img_ds = dataset_ops.Dataset.zip((img_ds, label_ds))

    return img_ds


def iter_valid_files(directory, follow_links):
    walk = os.walk(directory, followlinks=follow_links)
    for root, _, files in sorted(walk, key=lambda x: x[0]):
        for fname in sorted(files):
            if fname.lower().endswith(WHITELIST_FORMATS):
                yield root, fname


def list_labeled_images_in_directory(directory, class_indices, follow_links):
    """Recursively walks directory and list image paths and their class index.

  Arguments:
    directory: string, target directory.
    class_indices: dict mapping class names to their index.
    follow_links: boolean, whether to recursively follow subdirectories
      (if False, we only list top-level images in `directory`).

  Returns:
    tuple `(labels, filenames)`. `labels` is a list of integer
      labels and `filenames` is a list of relative image paths corresponding
      to these labels.
  """
    dirname = os.path.basename(directory)
    valid_files = iter_valid_files(directory, follow_links)
    labels = []
    filenames = []
    for root, fname in valid_files:
        labels.append(class_indices[dirname])
        absolute_path = os.path.join(root, fname)
        relative_path = os.path.join(dirname,
                                     os.path.relpath(absolute_path, directory))
        filenames.append(relative_path)
    return labels, filenames


def path_to_image(path,
                  image_size,
                  num_channels,
                  interpolation,
                  scale_depth=False):
    """Modified to preprocess rgb-d data from the washington dataset for use with a pretrained resnet."""
    img = io_ops.read_file(path)
    img = image_ops.decode_png(img, channels=num_channels)
    if num_channels == 3:
        tf.keras.applications.imagenet_utils.preprocess_input(img)
    if scale_depth and num_channels == 1:
        img = tf.cast(img, tf.dtypes.float32)
        img /= max_depth

    return image_ops.resize_images_v2(img, image_size, method=interpolation)
