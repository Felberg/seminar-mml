from __future__ import print_function
from __future__ import absolute_import

import threading
import multiprocessing
import Queue

import numpy as np

import audio_utils
import augmentation


def data_generator(files_labels,
                   is_training,
                   num_classes,
                   sample_rate,
                   dur=None,
                   mode='raw',
                   n_fft=1024,
                   n_hops=None,
                   n_mel=80,
                   n_mfcc=32,
                   add_same_class=True,
                   add_random=True,
                   noise_label=None):
    """Generator for different audio representations."""
    filelist_copy = files_labels[:]
    num_iter = 0
    while True:
        if num_iter > 0 and not is_training:
            yield None
        num_iter += 1
        if is_training:
            np.random.shuffle(filelist_copy)

        if is_training:
            if dur:
                buff_size = 64
            else:
                buff_size = 16
            if add_same_class:
                same_class_buffers = [[] for _ in range(num_classes)]
            elif add_random:
                random_buffer = []
            if noise_label is not None:
                noise_buffer = []

        for f in filelist_copy:
            path = f[0]
            labels_ = f[1]

            fid = path.split("/")[-1].split(".")[0]

            audio = audio_utils.load_audio(path, sample_rate)
            if is_training:
                audio = augmentation.time_shift(audio)

            frames = [audio]
            if dur:
                frame_size = dur * sample_rate
                diff = len(audio) - frame_size
                if diff == 0:
                    #nothing
                    frames = frames
                elif abs(diff) > sample_rate // 10:
                    frames = audio_utils.cut_into_fragments(audio, frame_size)
                else:
                    if diff > 0:
                        audio = audio[diff // 2:]
                        audio = audio[:frame_size]
                    else:
                        audio = np.pad(audio, (0, abs(diff)), 'mean')
                    frames = [audio]

                for a in frames:
                    labels = labels_
                    # augmentation
                    if is_training:
                        # time and pitch shifting by random amount
                        a = augmentation.time_shift(a)
                        a = augmentation.pitch_shift(a)
                        a = audio_utils.normalize_wave(a)

                        # populate augmentation buffers
                        if add_same_class:
                            this_class_buffer = same_class_buffers[labels[0]]
                            # add yourto the buffer
                            add_audio = None
                            if len(this_class_buffer) >= buff_size:
                                # actually do augmentation
                                rand_index = np.random.randint(
                                    0, len(this_class_buffer))
                                add_audio = this_class_buffer[rand_index]
                                add_audio = augmentation.time_shift(add_audio)
                                add_audio = augmentation.pitch_shift(add_audio)

                            this_class_buffer.append(a)
                            if add_audio is not None:
                                alpha = np.random.uniform(0.1, 0.5)
                                a = audio_utils.add_waves(
                                    a, add_audio, alpha=alpha)

                        elif add_random:
                            add_audio = None
                            if len(random_buffer) >= buff_size:
                                # actually do augmentation
                                rand_index = np.random.randint(
                                    0, len(random_buffer))
                                add_audio, add_labels = this_class_buffer[
                                    rand_index]
                                add_audio = augmentation.time_shift(add_audio)
                                add_audio = augmentation.pitch_shift(add_audio)

                            random_buffer.append([a, labels])
                            if add_audio is not None:
                                alpha = np.random.uniform(0.1, 0.5)
                                a = audio_utils.add_waves(
                                    a, add_audio, alpha=alpha)

                                # adjust labels
                                labels += add_labels

                        if noise_label:
                            if labels[0] == noise_label:
                                noise_buffer.append(a)
                            add_audio = None
                            if len(noise_buffer) >= buff_size:
                                # actually do augmentation
                                for i in range(3):
                                    rand_index = np.random.randint(
                                        0, len(noise_buffer))
                                    add_audio = this_class_buffer[rand_index]
                                    add_audio = augmentation.time_shift(
                                        add_audio)
                                    add_audio = augmentation.pitch_shift(
                                        add_audio)
                                    alpha = np.random.uniform(0.1, 0.4)
                                    a = audio_utils.add_waves(
                                        a, add_audio, alpha=alpha)

                        # if buffers not "full enough" continue until they are
                        # if they are "full enough" pop random element
                        if add_same_class:
                            this_class_buffer = same_class_buffers[labels[0]]
                            if len(this_class_buffer) <= buff_size:
                                continue
                            else:
                                rand_index = np.random.randint(
                                    0, len(this_class_buffer))
                                this_class_buffer.pop(rand_index)
                        elif add_random:
                            if len(random_buffer) <= buff_size:
                                continue
                            else:
                                rand_index = np.random.randint(
                                    0, len(random_buffer))
                                random_buffer.pop(rand_index)
                        if noise_label:
                            if len(noise_buffer) <= buff_size:
                                continue
                            else:
                                rand_index = np.random.randint(
                                    0, len(noise_buffer))
                                noise_buffer.pop(rand_index)

                    # compute output format
                    if mode == 'raw':
                        # renormalize just to make sure ..
                        a = audio_utils.normalize_wave(a)
                    elif mode == 'logamp':
                        a = audio_utils.logspec(
                            (audio_utils.compute_spectrogram(
                                a, n_fft=n_fft, n_hops=n_hops)))
                    elif mode == 'mel':
                        a = audio_utils.compute_melspectrogram(
                            a,
                            sample_rate,
                            n_fft=n_fft,
                            n_hops=n_hops,
                            n_mel=n_mel)
                    else:
                        a = audio_utils.compute_mfcc(
                            a,
                            sample_rate,
                            n_fft=n_fft,
                            n_hops=None,
                            n_mfcc=n_mfcc)

                    # update final label representation
                    if num_classes == 2:
                        if 1 in labels:
                            labels = [1]
                        else:
                            labels = [0]
                    else:
                        # one hot encode
                        one_hot = np.zeros([num_classes])
                        for l in labels:
                            one_hot[l] = 1
                            labels = one_hot

                    out_a = np.array(a)
                    out_a = out_a[..., np.newaxis]
                    out_l = np.array(labels).astype(np.float32)
                    out_n = np.array([fid], dtype='string')

                    yield out_a, out_l, out_n

                    # if training only use one of the frames to prevent
                    # 'biased' batches
                    if is_training:
                        break


def batch_generator(batch_size,
                    files_labels,
                    is_training,
                    num_classes,
                    sample_rate,
                    dur=None,
                    mode='raw',
                    n_fft=1024,
                    n_hops=None,
                    n_mel=80,
                    n_mfcc=32,
                    add_same_class=True,
                    add_random=True,
                    noise_label=None):
    """Generates audio batches."""
    single_gen = data_generator(
        files_labels, is_training, num_classes, sample_rate, dur, mode, n_fft,
        n_hops, n_mel, n_mfcc, add_same_class, add_random, noise_label)
    while True:
        a_batch = []
        l_batch = []
        n_batch = []
        while len(a_batch) < batch_size:
            out_a, out_l, out_n = next(single_gen)
            a_batch.append(out_a)
            l_batch.append(out_l)
            n_batch.append(out_n)
        yield np.array(a_batch), np.array(l_batch), np.array(n_batch)


class GeneratorQueue(object):
    """Queue using generators supporting threading and multiprocessing."""

    def __init__(self, generator, multiprocessing=True):
        self._generator = generator
        self._multiprocessing = multiprocessing
        self._stop_event = None
        self._threads = []
        self.queue = None

    def start(self, n_threads=1, capacity=128):
        def gen_task():
            while not self._stop_event.is_set():
                try:
                    if self._multiprocessing or self.queue.qsize() < capacity:
                        gen_out = next(self._generator)
                        self.queue.put(gen_out)
                except Exception:
                    self._stop_event.set()
                    raise

        if self._multiprocessing:
            self.queue = multiprocessing.Queue(maxsize=capacity)
            self._stop_event = multiprocessing.Event()
        else:
            self.queue = Queue.Queue()
            self._stop_event = threading.Event()

        for _ in range(n_threads):
            if self._multiprocessing:
                np.random.seed(None)
                thread = multiprocessing.Process(target=gen_task)
                thread.daemon = True
            else:
                thread = threading.Thread(target=gen_task)
            self._threads.append(thread)
            thread.start()

    def stop(self):
        if self._stop_event is None:
            print("can't stop if not running")
            return
        self._stop_event.set()

        for t in self._threads:
            if t.is_alive():
                if self._multiprocessing:
                    t.terminate()
                else:
                    t.join()

        if self._multiprocessing:
            self.queue.close()

        self._threads = []
        self._stop_event = None
        self.queue = None

    def get(self):
        while not self._stop_event.is_set():
            if not self.queue.empty():
                outputs = self.queue.get()
                if outputs is None:
                    raise StopIteration
                yield outputs
