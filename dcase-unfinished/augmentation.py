from __future__ import absolute_import
from __future__ import print_function
from __future__ import division

import numpy as np
import audio_utils
import tensorflow as tf


def time_shift(x):
    """Shifts waveform or spectrogram by a random amount in the time axis.

    Keyword Arguments:
    x -- input audio
    """
    ndim = x.ndim
    tdim = ndim - 1
    shift = np.random.randint(0, x.shape[tdim])
    return np.roll(x, shift, axis=tdim)


def pitch_shift_spectrogram(spec):
    """Pitch shifts spectrogram by shifting frequency axis up or down.

    Keyword Arguments:
    spec -- input spectrogram
    """

    p = spec.shape[0]
    max_pitch_shift = p // 20
    shift = np.random.randint(-max_pitch_shift, max_pitch_shift)
    shifted = np.roll(spec, shift, axis=0)

    # prevent 'overflowing' of low/high frequencies
    if shift > 0:
        shifted[:shift, :] = 0
    else:
        shifted[p+shift:, :] = 0
    return shifted


def pitch_shift_waveform(wave):
    """Pitch shifts audio.
    Keyword Arguments:
    wave -- input waveform
    """

    s = audio_utils.compute_spectrogram(wave)
    s = pitch_shift_spectrogram(s)

    return audio_utils.wave_from_spectrogram(s, len(wave))


def pitch_shift(x):
    if x.ndim == 1:
        return pitch_shift_waveform(x)
    elif x.ndim == 2:
        return pitch_shift_spectrogram(x)
    else:
        raise ValueError('Input has the be 1- or 2-D.')
