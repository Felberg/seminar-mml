from __future__ import division
from __future__ import absolute_import
from __future__ import print_function

import librosa
import numpy as np


def load_audio(path, sample_rate, mono=True, normalize=True, dtype=np.float32):
    """Loading audio file via librosa library.

    Keyword Arguments:
    path        -- path to file
    sample_rate -- sample rate which the audio gets sampled to
    mono        -- whether or not to ensure that audio is loaded in mono
    normalize   -- whether or not to peak normalize audio
    duration    -- duration (in sec) to cut/pad audio to. If None load file as it is
    """

    # when interrupted sometimes throws weird exception/error that then blocks read thread
    try:
        out, _ = librosa.load(path, sr=sample_rate, mono=mono, dtype=dtype)
    except:
        out = np.zeros((1, ))

    # librosa.util.normalize doesn't preserve dtype
    t = out.dtype
    if normalize:
        out = librosa.util.normalize(out, norm=np.inf)
        out = out.astype(t)

    return out


def write_audio(path, audio, sr, norm=False):
    librosa.output.write_wav(path, audio, sr, norm=norm)


def normalize_wave(a):
    return librosa.util.normalize(a, norm=np.inf).astype(a.dtype)


def cut_into_fragments(x, frag_size):
    """Cuts waveform into fragments of size frag_size"""
    x_len = len(x)

    mod = (x_len % frag_size)
    if mod == 0:
        return np.split(x, x_len // frag_size)

    # repeat
    num_even = frag_size - mod
    n_tiles = 2
    if x_len < frag_size:
        n_tiles = int(np.ceil(frag_size / x_len))
    rep_x = np.tile(x, n_tiles)
    rep_x = rep_x[:x_len + num_even]

    return np.split(rep_x, len(rep_x) // frag_size)


def add_waves(a, b, alpha=0.5):
    """Adds to waveforms a and b. Returns waveform of size a"""
    # make b equal to a in length
    a_len = len(a)
    b_len = len(b)

    # if b is shorter repeat
    if b_len < a_len:
        n_tiles = int(np.ceil(a_len / b_len))
        b = np.tile(b, n_tiles)

    # cut to size of a
    b = b[:a_len]

    # add
    out = (1 - alpha) * a + alpha * b

    # renormalize
    out = normalize_wave(out)
    return out


def compute_spectrogram(audio, n_fft=1024, n_hops=None, win_fn=np.hanning):
    """Computes the spectrogram of an audio file via STFT (Short-Time Fourier Transform)
    using librosa.

    Keyword Arguments:
    audio   -- input audio vector
    n_fft   -- FFT window size (default 1024)
    n_hops  -- Number of Frames between STFT columns. Defaults to n_fft / 4.
    win_fn  -- the window function to be used in the STFT computation (default scipy.signal.hamming)
    """

    if n_hops is None:
        n_hops = n_fft // 4

    stft = librosa.stft(audio, n_fft, n_hops, window=win_fn)

    return stft


def logspec(s, dtype=np.float32):
    return librosa.amplitude_to_db(s).astype(dtype)


def compute_melspectrogram(audio,
                           sample_rate,
                           n_fft=1024,
                           n_hops=None,
                           n_mel=80,
                           log=True):
    """Computes log-power melspectrogram of audio"""

    if n_hops is None:
        n_hops = n_fft // 4

    S = librosa.feature.melspectrogram(
        y=audio, sr=sample_rate, n_fft=n_fft, hop_length=n_hops, n_mels=n_mel)
    if log:
        S = librosa.power_to_db(S)
    return S.astype(audio.dtype)


def compute_mfcc(audio, sample_rate, n_fft=1024, n_hops=None, n_mfcc=32):
    """Computes MFCC features of audio"""

    if n_hops is None:
        n_hops = n_fft // 4

    return librosa.feature.mfcc(
        y=audio, sr=sample_rate, n_mfcc=n_mfcc, n_fft=n_fft,
        hop_length=n_hops).astype(audio.dtype)


def wave_from_spectrogram(spec, length, n_fft=1024, n_hops=None):
    """Computes the waveform from the given spectrogram using Inverse-STFT.

    Keyword Arguments:
    spec    -- input spectrogram
    length  -- expected length of the output
    n_fft   -- (default 1024)
    n_hops  -- (default None -> n_hops=n_fft/4)
    """
    if n_hops is None:
        n_hops = n_fft // 4

    return librosa.util.fix_length(
        librosa.istft(spec, hop_length=n_hops), length)
