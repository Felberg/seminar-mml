import os
import csv
import shutil
import random
import argparse

import numpy as np

import audio_utils


def get_args():
    parser = argparse.ArgumentParser(description="Datasets")
    parser.add_argument("--data_dirs", nargs='+', type=str, default=None)
    parser.add_argument("--save_dir", type=str, default=None)
    parser.add_argument("--test_split", type=float, default=0.2)

    # parser.add_argument("--sample_rate", type=int, default=22050)
    # parser.add_argument("--mode", type=str, default='raw')
    # parser.add_argument("--n_fft", type=int, default=1024)
    # parser.add_argument("--n_hops", type=int, default=None)
    # parser.add_argument("--n_mel", type=int, default=80)
    # parser.add_argument("--n_mfcc", type=int, default=32)
    # parser.add_argument("--duration", type=int, default=None)

    return parser.parse_args()


def find_files(path, ftype, contains=None):
    """
    Return a list containing all the files with the relevant type(/ending)
    Keyword Arguments:
    path     -- string of path where to search files
    ftype    -- file type/ending that we are searching for
    contains -- only return if filename contains string. If 'None' ignore.
    """

    found_files = []
    for root, dirs, files in os.walk(path):
        for file in files:
            if file.endswith(ftype):
                if contains is None or contains in file:
                    found_files.append(os.path.join(root, file))

    return found_files


def make_dataset_washington(path, save_dir, test_split=0.2, random=False):
    """Restructures the washington dataset to be of a structure we can use with tensorflow/keras
    data pipeline functions

    Expects data to be in a format similar to this:
    main_dir/
    ...class_a/
    ......a_img.png
    ......a_images_1/
    ...class_b/
    .
    .
    .
    """
    if not os.path.exists(save_dir):
        os.mkdir(save_dir)

    rgb_dir = os.path.join(save_dir, "rgb")
    depth_dir = os.path.join(save_dir, "depth")
    if not os.path.exists(rgb_dir):
        os.mkdir(rgb_dir)
    if not os.path.exists(depth_dir):
        os.mkdir(depth_dir)

    train_dir_r = os.path.join(rgb_dir, "train")
    train_dir_d = os.path.join(depth_dir, "train")

    if not os.path.exists(train_dir_r):
        os.mkdir(train_dir_r)
    if not os.path.exists(train_dir_d):
        os.mkdir(train_dir_d)

    if test_split not in {0.0, None}:
        test_dir_d = os.path.join(depth_dir, "test")
        test_dir_r = os.path.join(rgb_dir, "test")

        if not os.path.exists(test_dir_r):
            os.mkdir(test_dir_r)
        if not os.path.exists(test_dir_d):
            os.mkdir(test_dir_d)

    for classes in os.listdir(path):
        print("Working on " + classes + " directory")
        # skip any files we may encounter in the main directory
        class_path = os.path.join(path, classes)
        if os.path.isfile(class_path):
            next

        new_dir_train_r = os.path.join(train_dir_r, classes)
        new_dir_train_d = os.path.join(train_dir_d, classes)
        if not os.path.exists(new_dir_train_r):
            os.mkdir(new_dir_train_r)
        if not os.path.exists(new_dir_train_d):
            os.mkdir(new_dir_train_d)

        all_images = find_files(class_path, ".png", contains="depth")

        if test_split not in {0.0, None}:
            new_dir_test_r = os.path.join(test_dir_r, classes)
            new_dir_test_d = os.path.join(test_dir_d, classes)
            if not os.path.exists(new_dir_test_r):
                os.mkdir(new_dir_test_r)
            if not os.path.exists(new_dir_test_d):
                os.mkdir(new_dir_test_d)

            test_images = []
            train_images = []
            if random:
                num_test = int(len(all_images) * test_split)
                random.shuffle(all_images)
                test_images = all_images[:num_test]
                all_images = all_images[num_test:]
            else:
                num_test_skip = int(1. / test_split)
                for i, x in enumerate(all_images):
                    if i % num_test_skip == 0:
                        test_images.append(x)
                    else:
                        train_images.append(x)

                all_images = train_images

            for image in test_images:
                fname = image.split("\\")[-1]

                shutil.copy2(image, os.path.join(new_dir_test_d, fname))
                shutil.copy2(
                    image.replace('depth', ''),
                    os.path.join(new_dir_test_r, fname.replace('depth', '')))

        for image in all_images:
            fname = image.split("\\")[-1]

            shutil.copy2(image, os.path.join(new_dir_train_d, fname))
            shutil.copy2(
                image.replace('depth', ''),
                os.path.join(new_dir_train_r, fname.replace('depth', '')))


if __name__ == '__main__':
    args = get_args()
    make_dataset_washington(args.data_dirs, args.save_dir, args.test_split,
                            args.random)
