# Seminar MML - Learning to fuse latent representations for multimodal data

Implementation of multimodal methods presented in [1], [2], [3] for use with the Washington RGB-D object dataset [4].

## Getting Started

### Prerequisites

Python and the packages in requirements.txt are needed to run the code.

### Washington Dataset

You will also need to obtain the Washington RGB-D dataset.

### Running the Code

You will need to prepare the Washington dataset first by running 

```
python make_datasets.py --data_dirs /path/to/dataset --save_dir /path/to/save/to --test_split (None or some factor by which to split the dataset)
```

You can then either train the networks individually by running ```train_depth.py``` or ```train_rgb.py```
or train the fusion models and their individual parts using ```train_washington.py```. 
Providing the path to the data and save destination should be enough. For more options look at the arguments available..

### Notes
Due to some complications I would not have been able to train the models on the DCASE audio scene classification task as I originally intendend. So development remains unfinished.
I still included the code in a seperate folder, if anyone is interested..

## References
[1]
Oyebade K. Oyedotun et. al. (2019).
Learning to fuse latent representations for multimodal data.
ICASSP 2019-2019 IEEE International Conference on Acoustics, Speech and Signal Processing (ICASSP).

[2]
Oier Mees et. al. (2017).
Choosing Smartly: Adaptive Multimodal Fusion for Object Detection in Changing Environments.
CoRR abs/1707.05733.

[3]
John Arevalo et. al. (2017).
Gated Multimodal Units for Information Fusion.

[4]
Kevin Lai et. al. (2011).
A large-scale hierarchical multi-view rgb-d object dataset.
2011 IEEE international conference on robotics and automation.
