from __future__ import division
from __future__ import absolute_import
from __future__ import print_function

import os
import models
import argparse
import tensorflow as tf
import multiprocessing

import numpy as np

from mod_image_pipeline import dataset_from_directory
from utils import Print, SummaryCallback


def get_args():
    parser = argparse.ArgumentParser(description="Train FusionModel")
    parser.add_argument("--data_dirs",
                        nargs='+',
                        type=str,
                        default=[
                            "/mnt/HDD/Uni/MML/washington/data/depth/train",
                            "/mnt/HDD/Uni/MML/washington/data/rgb/train"
                        ])
    parser.add_argument("--test_dirs",
                        nargs='+',
                        type=str,
                        default=[
                            "/mnt/HDD/Uni/MML/washington/data/depth/test",
                            "/mnt/HDD/Uni/MML/washington/data/rgb/test"
                        ])
    parser.add_argument("--save_dir",
                        type=str,
                        default='/mnt/HDD/Uni/MML/washington/full_moe_64x64')
    parser.add_argument(
        "--weights_depth",
        type=str,
        nargs='+',
        # default=None
        default=[
            '/mnt/HDD/Uni/MML/washington/full_gated_4500/depth_weights_fold_0.hdf5',
            '/mnt/HDD/Uni/MML/washington/full_gated_4500/depth_weights_fold_1.hdf5',
            '/mnt/HDD/Uni/MML/washington/full_gated_4500/depth_weights_fold_2.hdf5',
            '/mnt/HDD/Uni/MML/washington/full_gated_4500/depth_weights_fold_3.hdf5',
            '/mnt/HDD/Uni/MML/washington/full_gated_4500/depth_weights_fold_4.hdf5'
        ])
    parser.add_argument("--weights_rgb",
                        type=str,
                        nargs='+',
                        # default='imagenet')
                        default=['/mnt/HDD/Uni/MML/washington/rgb_weights_128x128/weights_fold_0.hdf5',
                                 '/mnt/HDD/Uni/MML/washington/rgb_weights_128x128/weights_fold_1.hdf5',
                                 '/mnt/HDD/Uni/MML/washington/rgb_weights_128x128/weights_fold_2.hdf5',
                                 '/mnt/HDD/Uni/MML/washington/rgb_weights_128x128/weights_fold_3.hdf5',
                                 '/mnt/HDD/Uni/MML/washington/rgb_weights_128x128/weights_fold_4.hdf5'])
    parser.add_argument("--weights", type=str, default=None)
    parser.add_argument("--epochs", type=int, default=400)
    parser.add_argument("--shuffle_seed", type=int, default=1337)
    parser.add_argument("--k_fold", type=int, default=5)
    parser.add_argument("--finetuned_rgb", default=False, action='store_true')
    parser.add_argument("--num_samples", type=int, default=4500)
    parser.add_argument("--fusion_method", type=str, default='moe')
    parser.add_argument("--skip_if_trained", type=bool, default=True)
    parser.add_argument("--fc1", type=int, default=500)
    parser.add_argument("--fc2", type=int, default=300)
    parser.add_argument("--rgb_model", type=str, default='resnet')

    return parser.parse_args()



if __name__ == '__main__':
    depth_size = (64, 64)
    rgb_size = (128, 128)

    args = get_args()
    data_dirs = args.data_dirs

    print(args.weights_depth)

    sseed = args.shuffle_seed
    if sseed is None:
        sseed = np.random.randint(1e6)

    checkpoint_filepath = args.save_dir

    if not os.path.exists(checkpoint_filepath):
        os.mkdir(checkpoint_filepath)

    global_shuffle = True
    if args.k_fold:
        global_shuffle = False

    train_ds, num_samples = dataset_from_directory(
        data_dirs,
        image_sizes=[depth_size, rgb_size],
        shuffle=global_shuffle,
        seed=sseed,
        num_samples=args.num_samples)
    test_ds, num_test = dataset_from_directory(
        args.test_dirs,
        image_sizes=[depth_size, rgb_size],
        shuffle=False,
        seed=sseed)
    test_ds = test_ds.batch(32)

    train_once = False
    k_fold = args.k_fold
    if k_fold is None:
        # if no k-fold cv is done train one model on 80% of the training samples and validate on the rest
        k_fold = 5
        train_once = True
        args.weights_depth = [args.weights_depth for i in range(k_fold)]
        args.weights_rgb = [args.weights_rgb for i in range(k_fold)]

    if k_fold:
        if args.weights_depth is None:
            args.weights_depth = [None for i in range(k_fold)]

        if args.weights_rgb is None:
            args.weights_rgb = ['imagenet' for i in range(k_fold)]
        elif not isinstance(args.weights_rgb, list):
            args.weights_rgb = [args.weights_rgb for i in range(k_fold)]

        if len(args.weights_depth) != k_fold:
            print(
                "Warning! Number of depth model weights and crossvalidation models mismatch. Training the rest from scratch, orremoving them."
            )
            args.weights_depth = args.weights_depth[:k_fold]
            missing = k_fold - len(args.weights_depth)
            args.weights_depth = args.weights_depth + [
                None for m in range(missing)
            ]

        if len(args.weights_rgb) != k_fold:
            print(
                "Warning! Number of rgb model weights and crossvalidation models mismatch. Training the rest from scratch, orremoving them."
            )
            args.weights_rgb = args.weights_rgb[:k_fold]
            missing = k_fold - len(args.weights_rgb)
            args.weights_rgb = args.weights_rgb + [
                'imagenet' for m in range(missing)
            ]
        """Make datasets..."""
        shards = [
            train_ds.shard(num_shards=k_fold, index=i) for i in range(k_fold)
        ]

        results_depth_val = []
        results_depth_test = []
        results_rgb_val = []
        results_rgb_test = []
        results_full_test = []
        results_full_val = []
        for k in range(k_fold):
            print(
                "--------------------Training starting for fold {}-----------------"
                .format(k))
            blub = 0
            if k == 0:
                blub = 1

            train_ds = shards[blub]
            for si in range(k_fold):
                if si != k and si != blub:
                    train_ds = train_ds.concatenate(shards[si])

            valid_ds = shards[k]

            # train datasets
            depth_train = train_ds.map(lambda x, z: (x[0], z)).shuffle(
                num_samples - int(num_samples / k_fold)).batch(32)
            rgb_train = train_ds.map(lambda x, z: (x[1], z)).shuffle(
                num_samples - int(num_samples / k_fold)).batch(32)

            depth_valid = valid_ds.map(lambda x, z: (x[0], z)).shuffle(
                int(num_samples / k_fold)).batch(32)
            rgb_valid = valid_ds.map(lambda x, z: (x[1], z)).shuffle(
                int(num_samples / k_fold)).batch(32)

            rgb_test = test_ds.map(lambda x, z: (x[1], z))
            depth_test = test_ds.map(lambda x, z: (x[0], z))

            train_ds = train_ds.shuffle(num_samples -
                                        int(num_samples / k_fold)).batch(32)
            valid_ds = valid_ds.shuffle(int(num_samples / k_fold)).batch(32)
            """Train depth model..."""
            depth_input_shape = (depth_size[0], depth_size[1], 1)
            rgb_input_shape = (rgb_size[0], rgb_size[1], 3)
            depth_input = tf.keras.layers.Input(shape=depth_input_shape)
            rgb_input = tf.keras.layers.Input(shape=rgb_input_shape)

            # reduce learning rate callback
            reduce_lr = tf.keras.callbacks.ReduceLROnPlateau(monitor='val_acc',
                                                             factor=0.4,
                                                             patience=6,
                                                             min_lr=0.00001)

            early_cb = tf.keras.callbacks.EarlyStopping(
                monitor='val_acc',
                patience=40,
                verbose=1,
                mode='auto',
                restore_best_weights=True)

            model_checkpoint_callback = tf.keras.callbacks.ModelCheckpoint(
                filepath=os.path.join(checkpoint_filepath,
                                      "depth_weights_fold_{}.hdf5".format(k)),
                save_weights_only=True,
                monitor='val_acc',
                mode='auto',
                verbose=1,
                save_best_only=True)

            # depth_model = models.DepthModel(weights=None,
            depth_model = models.DepthModel(weights=args.weights_depth[k],
                                            input_tensor=depth_input)

            opt = tf.keras.optimizers.Adam(learning_rate=0.003)

            depth_model.compile(optimizer=opt,
                                loss='categorical_crossentropy',
                                metrics=['acc'])

            if args.skip_if_trained == False or args.weights_depth[k] == None:

                print(
                    "--------------------Training depth model for fold {}-----------------"
                    .format(k))
                depth_model.fit(
                    depth_train,
                    epochs=args.epochs,
                    # steps_per_epoch=args.steps,
                    # validation_steps=args.valid_steps,
                    callbacks=[model_checkpoint_callback, reduce_lr, early_cb],
                    validation_data=depth_valid,
                    max_queue_size=100,
                    workers=4,
                    use_multiprocessing=True)

            print(
                "--------------------Evaluating depth model for fold {}-----------------"
                .format(k))
            # res_val = depth_model.evaluate(depth_valid,
            #                                verbose=1,
            #                                workers=4,
            #                                use_multiprocessing=True)
            # res_test = depth_model.evaluate(depth_test,
            #                                 verbose=1,
            #                                 workers=4,
            #                                 use_multiprocessing=True)
            res_val = 0
            res_test = 0
            results_depth_val.append(res_val)
            results_depth_test.append(res_test)
            """Make notop depth model"""
            depth_notop = tf.keras.Sequential(depth_model.layers[:-4])
            depth_notop.add(tf.keras.layers.Flatten())
            depth_notop.trainable = False

            """Train rgb model"""
            if args.rgb_model == 'resnet':
                model_func = tf.keras.applications.ResNet50
            elif args.rgb_model == 'vgg19':
                model_func = tf.keras.applications.VGG19
            else:
                model_func = tf.keras.applications.ResNet50
                print("--rgb_model not in {'resnet', 'vgg19'}.. using resnet")

            rgb_model = model_func(
                include_top=False,
                weights='imagenet',
                input_tensor=rgb_input,
                input_shape=[rgb_input_shape[0], rgb_input_shape[1], 3],
                pooling='avg')

            rgb_model.trainable = False

            # NOTE: a litte hacky ... not as was done in the paper but the only way for the gating to work is if
            #       both outputs have same dimension... depth model after flatten is of shape [?, 800]..
            #       -> make fully connected layer 2 have a dim of 800 instead of 700(paper)
            rgb_pre = tf.keras.Sequential([rgb_model])
            rgb_pre.add(tf.keras.layers.Dense(1000))
            rgb_pre.add(
                tf.keras.layers.Dense(depth_notop.output.shape[-1],
                                      name='rep'))
            rgb_pre.add(tf.keras.layers.Dense(51, activation='softmax'))

            if args.weights_rgb[k] not in {None, 'imagenet'}:
                rgb_pre.load_weights(args.weights_rgb[k])

            if args.finetuned_rgb:

                opt = tf.keras.optimizers.Adam(learning_rate=0.001)

                rgb_pre.compile(optimizer=opt,
                                loss='categorical_crossentropy',
                                metrics=['acc'])

                if args.skip_if_trained == False or args.weights_rgb[k] in {
                        None, 'imagenet'
                }:

                    print(
                        "--------------------Training rgb model for fold {}-----------------"
                        .format(k))
                    rgb_pre.fit(rgb_train,
                                epochs=args.epochs // 2,
                                validation_data=rgb_valid,
                                callbacks=[reduce_lr, early_cb],
                                max_queue_size=100,
                                workers=4,
                                use_multiprocessing=True)

                    rgb_pre.save(
                        os.path.join(checkpoint_filepath,
                                     "rgb_{}.hdf5".format(k)))
                # rgb_pre.trainable = False

                print(
                    "--------------------Evaluating rgb model for fold {}-----------------"
                    .format(k))
                # res_val = rgb_pre.evaluate(rgb_valid,
                #                            verbose=1,
                #                            workers=4,
                #                            use_multiprocessing=True)
                # res_test = rgb_pre.evaluate(rgb_test,
                #                             verbose=1,
                #                             workers=4,
                #                             use_multiprocessing=True)
                res_val = 0
                res_test = 0
                results_rgb_val.append(res_val)
                results_rgb_test.append(res_test)

            rgb_pre_notop = tf.keras.Sequential(rgb_pre.layers[:-1])

            summary_vals = []
            if args.fusion_method == 'moe':
                if not args.finetuned_rgb:
                    rgb_pre.trainable = False
                depth_model.trainable = False
                rgb_map = rgb_pre.get_layer('rep').output
                depth_map = depth_model.get_layer('flatten').output
                fusion_map = tf.keras.layers.Concatenate(name='fusion_rep')(
                    [rgb_map, depth_map])
                # gating network
                # NOTE: modified architecture a bit.. our input is a lot larger than the architecture used in the paper
                dense1 = tf.keras.layers.Dense(
                    800,
                    activation='relu',
                    kernel_initializer=tf.keras.initializers.GlorotNormal(),
                    name='gdense1')(fusion_map)
                dense2 = tf.keras.layers.Dense(
                    300,
                    activation='relu',
                    kernel_initializer=tf.keras.initializers.GlorotNormal(),
                    name='gdense2')(dense1)
                dense3 = tf.keras.layers.Dense(
                    2,
                    activation='softmax',
                    kernel_initializer=tf.keras.initializers.GlorotNormal(),
                    name='gating_probs')(dense2)

                # split
                g1_, g2_ = tf.split(dense3, 2, 1)
                summary_vals.append(tf.reduce_mean(g1_))
                summary_vals.append(tf.reduce_mean(g2_))
                # g1_ = Print("G1: ")(g1_)
                # g2_ = Print("G2: ")(g2_)
                g1 = tf.multiply(g1_, rgb_pre.output)
                g2 = tf.multiply(g2_, depth_model.output)
                fusion_prediction = g1 + g2
                # fusion_prediction = Print("Predictions: ")(fusion_prediction)

                full_model = tf.python.keras.engine.training.Model(
                    [depth_model.input, rgb_pre.input],
                    fusion_prediction,
                    name='fusion_model')
            else:
                if args.fusion_method == 'gated':
                    print("USING SIGMOID GATED FUSION")
                    gate = tf.keras.layers.Dense(depth_notop.output.shape[-1],
                                                 activation='sigmoid',
                                                 use_bias=True)(
                                                     depth_notop.output)
                    summary_vals.append(tf.reduce_mean(gate))
                    # gate = Print("G: ")(gate)
                    rgb_gated = (1.0 - gate) * rgb_pre_notop.output
                    depth_gated = gate * depth_notop.output
                    fusion = tf.keras.layers.Concatenate()(
                        [rgb_gated, depth_gated])
                elif args.fusion_method == 'gmu':
                    print("USING GMU FUSION")
                    h1 = tf.keras.layers.Dense(800, activation='tanh')(
                        depth_notop.output)
                    h2 = tf.keras.layers.Dense(800, activation='tanh')(
                        rgb_pre_notop.output)
                    concat_ = tf.keras.layers.Concatenate()(
                        [depth_notop.output, rgb_pre_notop.output])
                    z = tf.keras.layers.Dense(800,
                                              activation='sigmoid')(concat_)
                    summary_vals.append(tf.reduce_mean(z))
                    # z = Print("Z: ")(z)
                    fusion = z * h1 + (1.0 - z) * h2
                else:
                    print("USING NAIVE FUSION")
                    fusion = tf.keras.layers.Concatenate()(
                        [rgb_pre_notop.output, depth_notop.output])

                fusion_fc1 = tf.keras.layers.Dense(args.fc1)(fusion)
                fusion_fc2 = tf.keras.layers.Dense(args.fc2)(fusion_fc1)

                # NOTE: worse..
                # fusion_fc1 = tf.keras.layers.dense(1000)(fusion)
                # fusion_fc2 = tf.keras.layers.dense(500)(fusion_fc1)

                # NOTE: worse..
                # fusion_fc1 = tf.keras.layers.Dense(800)(fusion)
                # fusion_fc2 = tf.keras.layers.Dense(400)(fusion_fc1)

                fusion_prediction = tf.keras.layers.Dense(
                    51, activation='softmax', name='fusion_prds')(fusion_fc2)

                full_model = tf.python.keras.engine.training.Model(
                    [depth_notop.input, rgb_pre_notop.input],
                    fusion_prediction,
                    name='fusion_model')

            full_model.summary()

            opt = tf.keras.optimizers.Adam(learning_rate=0.001)
            full_model.compile(optimizer=opt,
                               loss='categorical_crossentropy',
                               metrics=['acc'])

            model_checkpoint_callback = tf.keras.callbacks.ModelCheckpoint(
                # filepath=os.path.join(checkpoint_filepath, "full_model_{epoch:02d}-{val_acc:.2f}"),
                filepath=os.path.join(checkpoint_filepath,
                                      "full_model_{}".format(k)),
                save_weights_only=False,
                monitor='val_acc',
                mode='auto',
                verbose=1,
                save_best_only=True)

            logdir = checkpoint_filepath + "/logs"
            tb_callback = tf.keras.callbacks.TensorBoard(log_dir=logdir, histogram_freq=0, write_images=False)
            sum_callback = SummaryCallback(summary_vals, logdir)

            print(
                "--------------------Training full model for fold {}-----------------"
                .format(k))
            full_model.fit(
                train_ds,
                epochs=args.epochs,
                verbose=0,
                callbacks=[model_checkpoint_callback, reduce_lr, early_cb],
                # callbacks=[model_checkpoint_callback, reduce_lr, early_cb, sum_callback],
                validation_data=valid_ds,
                max_queue_size=100,
                workers=4,
                use_multiprocessing=True)

            print(
                "--------------------Evaluating full model for fold {}-----------------"
                .format(k))
            res_val = full_model.evaluate(valid_ds,
                                          verbose=0,
                                          workers=4,
                                          use_multiprocessing=True)
            res_test = full_model.evaluate(test_ds,
                                           verbose=0,
                                           workers=4,
                                           use_multiprocessing=True)
            results_full_val.append(res_val)
            results_full_test.append(res_test)

            if train_once:
                break

        with open(os.path.join(checkpoint_filepath, "results.txt"), 'w') as f:
            fstring = "TEST: loss, acc:\nDEPTH: " + str(
                results_depth_test
            ) + "\nRGB: " + str(results_rgb_test) + "\nFULL: " + str(
                results_full_test) + "\n\nVALID: loss, acc:\nDEPTH: " + str(
                    results_depth_val) + "\nRGB: " + str(
                        results_rgb_val) + "\nFULL: " + str(
                            results_full_val) + "\n"
            f.write(fstring)

        with open(os.path.join(checkpoint_filepath, "args.txt"), 'w') as f:
            argss = "Args: " + str(args)
            f.write(argss)
