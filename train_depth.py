from __future__ import division
from __future__ import absolute_import
from __future__ import print_function

import os
import models
import argparse
import tensorflow as tf
import numpy as np

from mod_image_pipeline import dataset_from_directory


def get_args():
    parser = argparse.ArgumentParser(description="Train DepthModel")
    parser.add_argument("--data_dir",
                        type=str,
                        default='/mnt/HDD/Uni/MML/washington/data/depth/train')
    parser.add_argument("--test_dir",
                        type=str,
                        default='/mnt/HDD/Uni/MML/washington/data/depth/test')
    parser.add_argument(
        "--save_dir",
        type=str,
        default='/mnt/HDD/Uni/MML/washington/depth_weights_larger')
    parser.add_argument("--weights", type=str, default=None)
    parser.add_argument("--epochs", type=int, default=400)
    parser.add_argument("--valid_split", type=float, default=0.2)
    parser.add_argument("--num_samples", type=int, default=None)
    parser.add_argument("--shuffle_seed", type=int, default=1337)
    parser.add_argument("--k_fold", type=int, default=None)

    return parser.parse_args()


if __name__ == '__main__':
    args = get_args()
    data_dir = os.path.expanduser(args.data_dir)
    test_dir = os.path.expanduser(args.test_dir)

    sseed = args.shuffle_seed
    if sseed is None:
        sseed = np.random.randint(1e6)

    # img_gen = tf.keras.preprocessing.image.ImageDataGenerator(rescale=1./255, validation_split=args.valid_portion)
    # img_gen = tf.keras.preprocessing.image.ImageDataGenerator(validation_split=args.valid_portion)
    shuff = True
    if args.k_fold:
        shuff = False

    train_ds, num_train = dataset_from_directory([data_dir],
                                                 color_modes=['grayscale'],
                                                 image_sizes=[(64, 64)],
                                                 interpolation=['nearest'],
                                                 num_samples=args.num_samples,
                                                 shuffle=shuff,
                                                 per_label_shuffle=False,
                                                 seed=sseed)
    test_ds, num_test = dataset_from_directory([test_dir],
                                               color_modes=['grayscale'],
                                               image_sizes=[(64, 64)],
                                               interpolation=['nearest'],
                                               shuffle=False)
    test_ds = test_ds.batch(32)

    checkpoint_filepath = args.save_dir

    if not os.path.exists(checkpoint_filepath):
        os.mkdir(checkpoint_filepath)

    if args.k_fold:
        shards = [
            train_ds.shard(num_shards=args.k_fold, index=i)
            for i in range(args.k_fold)
        ]

        results_val = []
        results_test = []
        for k in range(args.k_fold):
            blub = 0
            if k == 0:
                blub = 1

            train_ds = shards[blub]
            for si in range(args.k_fold):
                if si != k and si != blub:
                    train_ds = train_ds.concatenate(shards[si])

            valid_ds = shards[k]

            train_ds = train_ds.shuffle(num_train - int(num_train/5)).batch(32)
            valid_ds = valid_ds.shuffle(int(num_train/5)).batch(32)

            model = models.DepthModel(weights=args.weights)
            opt = tf.keras.optimizers.Adam(learning_rate=0.003)

            model.compile(optimizer=opt,
                          loss='categorical_crossentropy',
                          metrics=['acc'])

            reduce_lr = tf.keras.callbacks.ReduceLROnPlateau(monitor='val_acc',
                                                             factor=0.4,
                                                             patience=6,
                                                             min_lr=0.00001)

            early_cb = tf.keras.callbacks.EarlyStopping(
                monitor='val_acc',
                patience=40,
                verbose=1,
                mode='auto',
                restore_best_weights=True)

            model_checkpoint_callback = tf.keras.callbacks.ModelCheckpoint(
                filepath=os.path.join(checkpoint_filepath,
                                      "weights_fold_{}.hdf5".format(k)),
                save_weights_only=True,
                monitor='val_acc',
                mode='auto',
                verbose=1,
                save_best_only=True)

            model.fit(
                train_ds,
                epochs=args.epochs,
                # steps_per_epoch=args.steps,
                # validation_steps=args.valid_steps,
                callbacks=[model_checkpoint_callback, reduce_lr, early_cb],
                validation_data=valid_ds,
                max_queue_size=100,
                workers=4,
                use_multiprocessing=True)

            res_val = model.evaluate(valid_ds,
                                     verbose=1,
                                     workers=4,
                                     use_multiprocessing=True)

            res_test = model.evaluate(test_ds,
                                      verbose=1,
                                      workers=4,
                                      use_multiprocessing=True)
            results_val.append(res_val)
            results_test.append(res_test)
            print(res_val)
            print(res_test)
            fstring = "TEST: loss, acc: " + str(
                res_test) + "\nVAL: loss,acc: " + str(res_val)
            with open(
                    os.path.join(checkpoint_filepath,
                                 "results_{}.txt".format(k)), 'w') as f:
                f.write(fstring)

        print("Val:")
        print(results_val)
        print("Test:")
        print(results_test)
        fstring = "TEST: loss, acc: " + str(
            results_test) + "\nVAL: loss,acc: " + str(results_val)
        with open(os.path.join(checkpoint_filepath, "results.txt".format(k)),
                  'w') as f:
            f.write(fstring)

    else:
        num_valid = int(num_train * args.valid_split)
        valid_ds = train_ds.take(num_valid).shuffle(32 * 8).batch(32)
        train_ds = train_ds.skip(num_valid).shuffle(32 * 8).batch(32)

        train_ds = train_ds.prefetch(tf.data.experimental.AUTOTUNE)
        valid_ds = valid_ds.prefetch(tf.data.experimental.AUTOTUNE)

        model = models.DepthModel(weights=args.weights)

        opt = tf.keras.optimizers.Adam(learning_rate=0.003)
        model.compile(optimizer=opt,
                      loss='categorical_crossentropy',
                      metrics=['acc'])

        model.summary()

        reduce_lr = tf.keras.callbacks.ReduceLROnPlateau(monitor='val_acc',
                                                         factor=0.4,
                                                         patience=6,
                                                         min_lr=0.00001)

        early_cb = tf.keras.callbacks.EarlyStopping(
            monitor='val_loss',
            patience=40,
            verbose=1,
            mode='auto',
            restore_best_weights=True)

        model_checkpoint_callback = tf.keras.callbacks.ModelCheckpoint(
            filepath=os.path.join(checkpoint_filepath,
                                  "weights.{epoch:02d}-{val_acc:.2f}.hdf5"),
            save_weights_only=True,
            monitor='val_acc',
            mode='auto',
            verbose=1,
            save_best_only=True)

        model.fit(train_ds,
                  epochs=args.epochs,
                  callbacks=[
                      model_checkpoint_callback,
                      reduce_lr, early_cb
                  ],
                  validation_data=valid_ds,
                  max_queue_size=100,
                  workers=4,
                  use_multiprocessing=True)

        res = model.evaluate(test_ds,
                             verbose=1,
                             workers=4,
                             use_multiprocessing=True)
        print("loss, acc: ",
              res,
              file=os.path.join(checkpoint_filepath, 'results.txt'))
